﻿using UnityEngine;
using System.Collections;

public class NetworkRigidbody : MonoBehaviour
{

    public double m_InterpolationBackTime = 0.1;
    public double m_ExtrapolationLimit = 0.5;
    private float interpolationConstant = 0.01f;        // &lt;---- This was originally .1, but I had to make it .01 to really get rid of the jitter

    internal struct State
    {
        internal double timestamp;
        internal Vector3 pos;
        internal Vector3 velocity;
        internal Quaternion rot;
        internal Vector3 angularVelocity;
        internal float yaw;
        internal float pitch;
        internal float roll;
        internal float speed;
    }

    // We store twenty states with "playback" information
    State[] m_BufferedState = new State[20];
        // Keep track of what slots are used
        int m_TimestampCount;
    PhotonView _pv;
    PlayerControl _pc;
    ShipEngine _se;
    float queuedyaw;
    float queuedpitch;
    float queuedroll;
    float lastsentyaw;
    float lastsentpitch;
    float lastsentroll;

    void Awake()
    {
        _pv = (PhotonView)gameObject.GetComponent("PhotonView");         // &lt;--- used to check if networkView.isMine
        _pc = (PlayerControl)gameObject.GetComponent("PlayerControl");   // &lt;--- Used to get yaw, potch, roll inputs
        _se = (ShipEngine)gameObject.GetComponent("ShipEngine");         // &lt;--- Used to get throttle/speed setting

        enabled = !_pv.isMine;
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        // Send data to server
        if (stream.isWriting)
        {
            Vector3 pos = transform.position;
            Quaternion rot = transform.rotation;
            Vector3 velocity = rigidbody.velocity;
            Vector3 angularVelocity = rigidbody.angularVelocity;
            float yaw = queuedyaw;                                  // &lt;--- In the FixedUpdate below I keep track of max value between network sends, then send the max
            float pitch = queuedpitch;
            float roll = queuedroll;
            float speed = _se.shipspeed;
            lastsentyaw = queuedyaw;
            lastsentpitch = queuedpitch;
            lastsentroll = queuedroll;

            queuedyaw = 0;                                          // &lt;--- Reset
            queuedpitch = 0;
            queuedroll = 0;

            stream.Serialize(ref pos);
            stream.Serialize(ref velocity);
            stream.Serialize(ref rot);
            stream.Serialize(ref angularVelocity);
            stream.Serialize(ref yaw);
            stream.Serialize(ref pitch);
            stream.Serialize(ref roll);
            stream.Serialize(ref speed);
        }
        // Read data from remote client
        else
        {
            Vector3 pos = Vector3.zero;
            Vector3 velocity = Vector3.zero;
            Quaternion rot = Quaternion.identity;
            Vector3 angularVelocity = Vector3.zero;
            float yaw = 0f;
            float pitch = 0f;
            float roll = (short)0;
            float speed = 0f;

            stream.Serialize(ref pos);
            stream.Serialize(ref velocity);
            stream.Serialize(ref rot);
            stream.Serialize(ref angularVelocity);
            stream.Serialize(ref yaw);
            stream.Serialize(ref pitch);
            stream.Serialize(ref roll);
            stream.Serialize(ref speed);

            // Shift the buffer sideways, deleting state 20
            for (int i = m_BufferedState.Length - 1; i >= 1; i--)
                {
                m_BufferedState [i] = m_BufferedState[i-1];
                }

            // Record current state in slot 0
            State state;
            state.timestamp = info.timestamp;
            state.pos = pos;
            state.velocity = velocity;
            state.rot = rot;
            state.angularVelocity = angularVelocity;
            state.yaw = yaw;
            state.pitch = pitch;
            state.roll = roll;
            state.speed = speed;
            m_BufferedState [0] = state;
     
                // Update used slot count, however never exceed the buffer size
                // Slots aren't actually freed so this just makes sure the buffer is
                // filled up and that uninitalized slots aren't used.
                m_TimestampCount = Mathf.Min(m_TimestampCount + 1, m_BufferedState.Length);

            // Check if states are in order, if it is inconsistent you could reshuffel or
            // drop the out-of-order state. Nothing is done here
            for (int i = 0; i < m_TimestampCount - 1; i++)
                {
                if (m_BufferedState [i].timestamp < m_BufferedState[i+1].timestamp)
                        Debug.Log("State inconsistent");
            }
        }
    }

    // We have a window of interpolationBackTime where we basically play
    // By having interpolationBackTime the average ping, you will usually use interpolation.
    // And only if no more data arrives we will use extra polation
    void FixedUpdate()
    {

        // If we're the owning player, not remote, then grab which is the largest for this net period
        if (_pv.isMine)
        {
            if (Mathf.Abs(_pc.prevMouseYaw) > Mathf.Abs(queuedyaw) )
		            queuedyaw = _pc.prevMouseYaw;
            if (Mathf.Abs(_pc.prevMousePitch) > Mathf.Abs(queuedpitch) )
            		queuedpitch = _pc.prevMousePitch;
            if (Mathf.Abs(_pc.prevMouseRoll) > Mathf.Abs(queuedroll) )
    	        	queuedroll = _pc.prevMouseRoll;

            _pc.prevMouseYaw = 0;
            _pc.prevMousePitch = 0;
            _pc.prevMouseRoll = 0;
        }

        if (m_BufferedState [0].timestamp == 0)
                return;

        // This is the target playback time of the rigid body
        double interpolationTime = PhotonNetwork.time - m_InterpolationBackTime;

        // Use interpolation if the target playback time is present in the buffer
        if (m_BufferedState [0].timestamp > interpolationTime)
            {
            //Debug.Log("Performing Smoothing");

            // Go through buffer and find correct state to play back
            for (int i = 1; i < m_TimestampCount; i++)
                {
                if (m_BufferedState [i].timestamp <= interpolationTime || i == m_TimestampCount-1)
                    {
                    // The state one slot newer (&lt;100ms) than the best playback state
                    State rhs = m_BufferedState [i-1];
                        // The best playback state (closest to 100 ms old (default time))
                        State lhs = m_BufferedState [i];
     
                        // Use the time between the two slots to determine if interpolation is necessary
                        double length = rhs.timestamp - lhs.timestamp;
                    float t = 0.0F;
                    // As the time difference gets closer to 100 ms t gets closer to 1 in
                    // which case rhs is only used
                    // Example:
                    // Time is 10.000, so sampleTime is 9.900
                    // lhs.time is 9.910 rhs.time is 9.980 length is 0.070
                    // t is 9.900 - 9.910 / 0.070 = 0.14. So it uses 14% of rhs, 86% of lhs
                    if (length > 0.0001){
                        t = (float)((interpolationTime - lhs.timestamp) / length);
                    }

                    // if t=0 =&gt; lhs is used directly
                    transform.localPosition =
                        Vector3.Lerp(transform.localPosition,
                                      Vector3.Lerp(lhs.pos, rhs.pos, t),
                                      interpolationConstant);
                    transform.localRotation =
                        Quaternion.Slerp(transform.localRotation,
                                          Quaternion.Slerp(lhs.rot, rhs.rot, t),
                                          interpolationConstant);
                    rigidbody.velocity =
                        Vector3.Lerp(rigidbody.velocity,
                                      Vector3.Lerp(lhs.velocity, rhs.velocity, t),
                                      interpolationConstant);
                    rigidbody.angularVelocity =
                        Vector3.Lerp(rigidbody.angularVelocity,
                                      Vector3.Lerp(lhs.angularVelocity, rhs.angularVelocity, t),
                                      interpolationConstant);

                    // Normally on the remote avatar we wouldn't process controls, but for this to perfectly smooth
                    // we set the controls on the avatar (we don't read them from Input, and allow the controls to update
                    // this is not fully required but helps further improve smoothing
                    _pc.mouseYaw = Mathf.Lerp(lhs.yaw, rhs.yaw, t);
                    _pc.mousePitch = Mathf.Lerp(lhs.pitch, rhs.pitch, t);
                    _pc.mouseRoll = Mathf.Lerp(lhs.roll, rhs.roll, t);
                    _se.shipspeed = Mathf.Lerp(lhs.speed, rhs.speed, t);
                    return;
                }
            }
        }
        else
        {
            //Debug.Log("Performing Prediction");

            float dt = (float)(PhotonNetwork.time - m_BufferedState[0].timestamp);
                Vector3 extra_pos = m_BufferedState [0].pos + m_BufferedState[0].velocity * dt;
     
                float angle = m_BufferedState [0].angularVelocity.magnitude;
                Vector3 axis = m_BufferedState [0].angularVelocity / angle;
                Quaternion extra_rot = m_BufferedState [0].rot * Quaternion.AngleAxis (angle * dt, axis);
     
                transform.localPosition = Vector3.Lerp(transform.localPosition, extra_pos, interpolationConstant);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, extra_rot, interpolationConstant);

            rigidbody.velocity = Vector3.Lerp(rigidbody.velocity, m_BufferedState [0].velocity, interpolationConstant);
                rigidbody.angularVelocity = Vector3.Lerp(rigidbody.angularVelocity, m_BufferedState [0].angularVelocity, interpolationConstant);
     
				_pc.mouseYaw = m_BufferedState [0].yaw;
				_pc.mousePitch = m_BufferedState[0].pitch;
				_pc.mouseRoll = m_BufferedState [0].roll;
				_se.shipspeed = m_BufferedState [0].speed;
            }
    }
}