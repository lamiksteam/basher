﻿using UnityEngine;
using System.Collections;

public class MouseLook : MonoBehaviour {
	
	public float rotationSpeed;
	private Quaternion targetRotation;
	
	// Use this for initialization
	void Start () {
		
	}
	//Quaternion.Euler(180, 0, 180)
	// Update is called once per frame
	void Update () {
		Vector3 mousePos = Input.mousePosition;
		mousePos = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x,mousePos.y,Camera.main.transform.position.y - transform.position.y));
		targetRotation = Quaternion.LookRotation(mousePos - new Vector3(transform.position.x,0,transform.position.z));
		transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y,targetRotation.eulerAngles.y,rotationSpeed * Time.deltaTime);
	}
}
