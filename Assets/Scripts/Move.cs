﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {
	public int speed;
	public int maxSpeed;
	string currentGun;
	private Quaternion rotation;
	private Vector3 position;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
					
	}

	void FixedUpdate(){
		GetComponent<Rigidbody>().angularDrag = 0;
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		if (moveHorizontal == 0 && moveVertical == 0 && GetComponent<Rigidbody>().velocity.magnitude >= 0.0001 && GetComponent<Rigidbody>().velocity.magnitude < 5000)
		{
			GetComponent<Rigidbody>().angularDrag = 1000000;
		}
		Vector3 movement = new Vector3 (moveHorizontal, 0f, moveVertical);
		if (GetComponent<Rigidbody> ().velocity.magnitude <= maxSpeed || GetComponent<Rigidbody> ().velocity.magnitude > 5000) {
			GetComponent<Rigidbody>().AddForce (movement * speed * Time.deltaTime);
		}
	}	
}
