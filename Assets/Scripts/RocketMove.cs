using UnityEngine;
using System.Collections;

public class RocketMove : Photon.MonoBehaviour {
	public int rocketSpeed;
	public int rocketDamage;
	public float liveTime;
	private float timeStamp;
	public GameObject explosion;
	[HideInInspector]
	public string rocketOwner;

	// Use this for initialization
	void Start () {
		timeStamp = Time.time + liveTime;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += transform.forward * rocketSpeed * Time.deltaTime;
		gameObject.GetComponent<Rigidbody> ().WakeUp ();
		if (timeStamp <= Time.time) {
			Destroy(gameObject);
		}

	}

	public void setOwner(string owner)
	{
		rocketOwner = owner;
		Debug.Log (rocketOwner);
	}
	
	void OnCollisionEnter(Collision collision){
		Instantiate (explosion, gameObject.transform.position, gameObject.transform.rotation);
		Destroy (gameObject);
	}
}
