﻿using UnityEngine;
using System.Collections;

public class Crosshair : MonoBehaviour 
{
	public Texture2D crosshairTexture; 
	public Rect position; 
	
	void Start() 
	{
		position = new Rect((Screen.width - crosshairTexture.width) / 2, (Screen.height - crosshairTexture.height) /2, crosshairTexture.width, crosshairTexture.height);
	}
	
	void OnGUI() 
	{
		Cursor.visible = false; 
		Vector2 mousePos = Event.current.mousePosition;
		GUI.DrawTexture( new Rect( mousePos.x - (crosshairTexture.width/2),
		                          mousePos.y - (crosshairTexture.height/2),
		                          crosshairTexture.width,
		                          crosshairTexture.height), crosshairTexture);
	}
}