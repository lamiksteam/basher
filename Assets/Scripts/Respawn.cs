﻿using UnityEngine;
using System.Collections;

public class Respawn : Photon.MonoBehaviour {

	private GameObject[] SpawnSpots;
	// Use this for initialization
	void Start () {
		if (photonView.isMine)
			gameObject.GetComponent<Camera> ().enabled = true;
		SpawnSpots = GameObject.FindGameObjectsWithTag ("SpawnSpot");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void CreatePlayer(){
		if(photonView.isMine){
		GameObject mySpawnSpot = SpawnSpots [UnityEngine.Random.Range (0, SpawnSpots.Length)];
		GameObject gm = PhotonNetwork.Instantiate ("Actor", mySpawnSpot.transform.position, mySpawnSpot.transform.rotation, 0);
		Transform tr = gm.transform.FindChild ("BasherCamera");
		tr.gameObject.SetActive (true);
		gm.GetComponentInChildren<Movement> ().enabled 	= true;
		gm.GetComponentInChildren<MouseLook> ().enabled = true;
		gm.GetComponentInChildren<HealthPoints> ().enabled = true;
        gm.GetComponent<Shooting>().enabled = true;
	  }
	}

	void OnGUI(){
		if (GUI.Button (new Rect (Screen.width / 2 - 125, Screen.height / 4, 250, 100), "Resurrection")) {
			CreatePlayer();
			PhotonNetwork.Destroy(gameObject);
		}

	}

}
