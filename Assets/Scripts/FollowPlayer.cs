﻿using UnityEngine;
using System.Collections;

public class FollowPlayer: MonoBehaviour {
	
	// The target we are following
	public  Transform target;
	// The distance in the x-z plane to the target
	public int distance = 10;
	private Plane plane = new Plane(Vector3.up, new Vector3(0f,0f,0f));
	private Ray ray;
	private float distanceForCamera;
	public float boundZ;
	private float oldZ;
	private Vector3 botRight;
		

	void Start(){
		Vector3 tmp = new Vector3 (transform.position.x, transform.position.y + distance, transform.position.z);
		transform.position = tmp;
		oldZ = 0;
	}
	
	void  LateUpdate (){
		if (!target)
			return;
		//Transform tr = gameObject.GetComponentInParent<Transform>();
		//if(((targetPoint.x  <= tr.position.x - square) || (targetPoint.x  >= tr.position.x +square)) || ((targetPoint.z  <= tr.position.z - square) || (targetPoint.z  >= tr.position.z +square)))
		//{
		//}

		ray = Camera.main.ViewportPointToRay(new Vector3(1,0,0)); // bottom right ray
		if (plane.Raycast(ray, out distanceForCamera)){
			botRight = ray.GetPoint(distanceForCamera);
			//Debug.Log(botRight.z);
		}
		if (botRight.z <= boundZ) {
			if(oldZ == 0)
				oldZ = target.position.z;
			transform.position = Vector3.Lerp (transform.position, new Vector3(target.position.x, target.position.y, oldZ) + new Vector3 (0f, (distance - target.position.y), -25f), 
			                                   Time.deltaTime * 5f);
			if(oldZ <= target.position.z)
				transform.position = Vector3.Lerp (transform.position, target.position + new Vector3 (0f, (distance - target.position.y), -25f), 
				                                  Time.deltaTime * 5f);
		} else {
			if(oldZ != 0)
				oldZ = 0;
			transform.position = Vector3.Lerp (transform.position, target.position + new Vector3 (0f, (distance - target.position.y), -25f), 
		                                  Time.deltaTime * 5f);
		}
	}
}
 