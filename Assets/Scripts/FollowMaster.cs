﻿using UnityEngine;
using System.Collections;

public class FollowMaster : MonoBehaviour {
	public Transform target;
	public float precisionX;
	public float precisionY;
	public float precisionZ;
	private Vector3 temp;
	public int precisionAngleX;
	public int precisionAngleY;
	public int precisionAngleZ;
	public Transform targetAngle;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		temp = new Vector3 (target.position.x + precisionX, target.position.y + precisionY, target.position.z + precisionZ);
		transform.position = temp;
		if (targetAngle) {
			transform.eulerAngles = new Vector3(targetAngle.eulerAngles.x + precisionAngleX, targetAngle.eulerAngles.y + precisionAngleY, targetAngle.eulerAngles.z + precisionAngleZ);
		}
	}
}
