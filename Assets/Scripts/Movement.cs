﻿using UnityEngine;

public class Movement : MonoBehaviour {
    public float maxSpeed;
    public float breakSpeed;
    public float accelarationSpeed;
    public float neutralSpeed;
    private float speed = 0f;
    private Vector3 moveDirection = Vector3.zero;

	// Update is called once per frame
	void Update () {
        //Feed moveDirection with input.
        if(speed < 0)
        {
            speed = 0;
        }

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        }
        //----------------------------------------------------------------------------------------------------------------------------   
        if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D) && speed > 0)
        {
            speed -= breakSpeed;
        }
        else if(!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D) && speed > 0)
        {
            speed -= breakSpeed;
        }
        else if((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) ) && speed <28)
		{
			speed += accelarationSpeed;        
		}
        else if(!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D) && speed > 0)
        {
            speed -= neutralSpeed;
        }
        transform.Translate(moveDirection.normalized * speed * Time.deltaTime, Space.World);
    }
}
