﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour
{
	
	// speed is the rate at which the object will rotate
	public float speed;
	public int square;

	void Start()
	{

	}


	void Awake()
	{

	}
	
	void LateUpdate()
	{  
		Super ();
	}
	
	void Super() 
	{

		// Generate a plane that intersects the transform's position with an upwards normal.
		Plane playerPlane = new Plane(Vector3.up, transform.position);
		
		// Generate a ray from the cursor position
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		
		// Determine the point where the cursor ray intersects the plane.
		// This will be the point that the object must look towards to be looking at the mouse.
		// Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
		//   then find the point along that ray that meets that distance.  This will be the point
		//   to look at.
		float hitdist = 0.0f;
		// If the ray is parallel to the plane, Raycast will return false.
		if (playerPlane.Raycast (ray, out hitdist)) 
		{
			// Get the point along the ray that hits the calculated distance.
			Vector3 targetPoint = ray.GetPoint(hitdist);
			// Determine the target rotation.  This is the rotation if the transform looks at the target point.
			Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
			Transform tr = gameObject.GetComponentInParent<Transform>();
			//Debug.Log (targetPoint.x.ToString() + " " + tr.position.x);
			//Debug.Log (targetPoint.z.ToString() + " " + tr.position.z);
			if(((targetPoint.x  <= tr.position.x - square) || (targetPoint.x  >= tr.position.x +square)) || ((targetPoint.z  <= tr.position.z - square) || (targetPoint.z  >= tr.position.z +square)))
			{
				transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation * Quaternion.Euler(180, 0, 180), speed * Time.deltaTime);
				//Debug.Log("working");
			}

			// Smoothly rotate towards the target point.
		}
	}
} 