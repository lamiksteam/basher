﻿using UnityEngine;
using System.Collections;

public class ParticleLifeTime : Photon.MonoBehaviour {
	public float liveTime;
	private float timeStamp;
	// Use this for initialization
	void Start () {
		timeStamp = Time.time + liveTime;
	}
	
	// Update is called once per frame
	void Update () {
		if (timeStamp <= Time.time) {
			Destroy(gameObject);
	}
	}
}
