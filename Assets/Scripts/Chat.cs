using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Chat : Photon.MonoBehaviour {
	
	public GUISkin Skin;   
	//public bl_RoomMenu m_Menu; 
	public int maxMessages = 10;   
	public GUIStyle stile;
	[HideInInspector]
	public bool showChat = false;
	//Private
	private List<string> messages = new List<string>();
	private Vector2 scrollPosition = Vector2.zero;
	private float m_alpha = 0;
	private string inputField = string.Empty;

	void Start(){
	}

	void Awake()
	{
		m_alpha = 0;
		showChat = false;
	}
	
	void Update()
	{
		if (this.m_alpha > 0)
		{
			this.m_alpha -= Time.deltaTime;// Fade
		}
	}
	
	public void OnGUI()
	{
		GUI.skin = Skin;
		if (this.showChat)
		{
			GUILayout.BeginArea(new Rect((float)2, Screen.height - 250, (float)450, (float)250), string.Empty, "box");
		}
		else
		{
			GUI.Label(new Rect(10, Screen.height - 30, 300, 30), "");
			GUI.color = new Color(1, 1, 1, m_alpha);
			GUILayout.BeginArea(new Rect((float)2, Screen.height - 187, (float)450, (float) 187), string.Empty, string.Empty);
			
		}
		this.scrollPosition = GUILayout.BeginScrollView(this.scrollPosition, "box");
		GUILayout.FlexibleSpace();
		if ((this.m_alpha > 1f) || this.showChat)
		{
			for (int i = 0; i < this.messages.Count; i++)
			{
				GUILayout.Label(this.messages[i].ToString(), this.stile);
			}
		}
		GUILayout.EndScrollView();
		if ((Event.current.type == EventType.KeyDown) && (Event.current.keyCode == KeyCode.KeypadEnter))
		{
			if (this.inputField.Length > 0)
			{
				object[] parameters = new object[] { this.inputField, PhotonNetwork.player.name };
				this.photonView.RPC("SendMessage", PhotonTargets.AllBufferedViaServer, parameters);
				this.inputField = string.Empty;
				this.showChat = false;
			}
		}
		if (this.showChat)
		{
			GUI.FocusControl("ChatField");
			GUI.SetNextControlName("ChatField");
			GUILayout.BeginHorizontal("box", new GUILayoutOption[0]);
			GUILayoutOption[] options = new GUILayoutOption[] { GUILayout.Width((float)360) };
			this.inputField = GUILayout.TextField(this.inputField, options);
			GUILayout.Space((float)3);
			GUILayoutOption[] optionArray2 = new GUILayoutOption[] { GUILayout.Width((float)70) };
			if (GUILayout.Button("<b>Send</b>", optionArray2) && (this.inputField.Length > 0))
			{
				this.showChat = false;
				object[] objArray2 = new object[] { this.inputField, PhotonNetwork.player.name };
				this.photonView.RPC("SendMessage", PhotonTargets.AllBufferedViaServer, objArray2);
				this.inputField = string.Empty;
			}     
			GUILayout.EndHorizontal();
		}
		else
		{
			GUI.FocusControl("");
		}
		GUILayout.EndArea();
		if ((((Event.current.type == EventType.KeyDown) && (Event.current.character == '\n')) && !this.showChat))
		{
			this.showChat = true;
		}
		else if (((Event.current.type == EventType.KeyDown) && (Event.current.character == '\n')) && (this.inputField.Length <= 0))
		{
			this.showChat = false;
		}
		else if (Event.current.type == EventType.KeyDown && Event.current.character == '\n' && this.inputField.Length > 0)
		{
			this.showChat = false;
			object[] objArray2 = new object[] { this.inputField, PhotonNetwork.player.name };
			this.photonView.RPC("SendMessage", PhotonTargets.AllBufferedViaServer, objArray2);
			this.inputField = string.Empty;
		}
	}
	
	[PunRPC]
	void SendMessage(string message, string senderName)
	{
		string str = string.Empty;
		this.scrollPosition.y = float.PositiveInfinity;
		if (senderName != null)
		{
			if (senderName == string.Empty)
			{
				str = "<b><color=orange>[SERVER]</color></b>";
			}
			else
			{
				str = ("<b><color=#FFAD33>" + senderName) + "</color></b>";
			}
		}
		if (this.messages.Count > this.maxMessages)
		{
			this.messages.RemoveAt(0);
		}
		this.messages.Add(((("<b>" + str) + ": ") + message) + "</b>");
		this.m_alpha = 7f;
	}
	
	[PunRPC]
	void SendKillMessage(string killer, string victim)
	{
		string str = string.Empty;
		this.scrollPosition.y = float.PositiveInfinity;
		str = ("<b><color=#ff0000>" + victim) + "</color></b>" + " killed by " + ("<b><color=#3caa3c>" + killer) + "</color></b>";
		if (this.messages.Count > this.maxMessages)
		{
			this.messages.RemoveAt(0);
		}
		this.messages.Add(str);
		this.m_alpha = 7f;
	}

	public void OnPhotonPlayerConnected(PhotonPlayer nPlayer){
		if (PhotonNetwork.isMasterClient) 
		{
			object[] parameters = new object[] { nPlayer.name + " has connected", string.Empty };
			this.photonView.RPC("SendMessage", PhotonTargets.All, parameters);
		}
	}

	public void OnPhotonPlayerDisconnected(PhotonPlayer nPlayer)
	{
		if (PhotonNetwork.isMasterClient)
		{
			object[] parameters = new object[] { nPlayer.name + " left game", string.Empty };
			this.photonView.RPC("SendMessage", PhotonTargets.All, parameters);
		}
	}

	public void OnKill(string killer, string victim)
	{
		photonView.RPC("SendKillMessage", PhotonTargets.All, killer, victim);
	}
	
}