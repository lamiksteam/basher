﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthPoints : Photon.MonoBehaviour {
	//public int maxHP;
	public int currentHP;
	public GameObject player;
	private string killer;
	public Slider HealthBar;
	private bool decreaseHP;
		
	// Use this for initialization
	void Start () {
		//currentHP = maxHP;
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.GetComponent<Rigidbody> ().WakeUp ();
		//HealthBar.transform.position = //transform.position + new Vector3(1, 13, 0);
		//Vector3.Lerp (HealthBar.transform.position, transform.position + new Vector3(0, 13, 0), 
		//              Time.deltaTime * 7f);
		//if (decreaseHP) {
		//	HealthBar.value -= 0.007f;
		//	if(HealthBar.value <= (float) currentHP/100)
		//		decreaseHP = false;
		//}
	}
	void OnGUI(){
		GUILayout.Space(30);
		GUILayout.Label("HP: " + getHP().ToString());
	}

	public int getHP()
	{
		return currentHP;
	}

	public void setHP(int newHP)
	{
		currentHP = newHP;
	}


	public void damageHP(int damage)
	{
		currentHP -= damage;
		decreaseHP = true;
		//HealthBar.value -=(float) damage / 100;

		OnHealthChanged ();
	}

	void OnCollisionEnter(Collision collision){
		if (GetComponentInParent<PhotonView> ().photonView.isMine && collision.collider.CompareTag ("Rocket")) {
			killer = collision.collider.GetComponent<RocketMove>().rocketOwner;
			damageHP (collision.gameObject.GetComponent<RocketMove> ().rocketDamage);
		}
	}

	public void OnHealthChanged(){
		if (currentHP <= 0) {
			currentHP = 0;
			//GameObject chat = GameObject.FindGameObjectWithTag("MainScripts");
			//string victim = PhotonNetwork.playerName;
			//chat.GetComponent<Chat>().OnKill(killer, victim);
			PhotonNetwork.Destroy(player);
			GameObject turnOnScript = PhotonNetwork.Instantiate("DeadCamera",player.transform.position + new Vector3(0, 70, 0), player.transform.rotation * Quaternion.Euler(70,0,0), 0);
			turnOnScript.GetComponent<Camera>().enabled = true;
			turnOnScript.GetComponent<Respawn>().enabled = true;

		}
	}
}
