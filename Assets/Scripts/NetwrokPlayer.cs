﻿using UnityEngine;
using System.Collections;

public class NetwrokPlayer : Photon.MonoBehaviour {
	
	Vector3 realPosition = Vector3.zero;
	Quaternion realRotation = Quaternion.identity;
	Vector3 oldPosition = Vector3.zero;

	Vector3 realPositionHat = Vector3.zero;
	Quaternion realRotationHat = Quaternion.identity;
	
	Vector3 realPositionBeard = Vector3.zero;
	Quaternion realRotationBeard = Quaternion.identity;
	
	Vector3 realPositionCenter = Vector3.zero;
	Quaternion realRotationCenter = Quaternion.identity;

	private float offsetTime;
	private double networkTime;
	private Vector3 realPositionR;
	private Quaternion realRotationR;
	private Vector3 realVelocityR;
	private Vector3 realVelocityRA;
	

	public GameObject player;
	public GameObject hat;
	public GameObject beard;
	public GameObject center;

	private float oldHealth;
	private float realVelocity;
	private int Health;
	private bool Sync;

	void Awake(){
		PhotonNetwork.playerName = (string) photonView.instantiationData[0];
	}
	// Use this for initialization
	void Start () {

		Health = player.GetComponent<HealthPoints> ().getHP ();
		Debug.Log(player.GetComponent<HealthPoints> ().getHP ().ToString());
		oldHealth = Health;
	}
	
	void Update() {
		if (!photonView.isMine) 
		{
			offsetTime += Time.deltaTime * 9f;
			//player.transform.position = Vector3.Lerp(player.transform.position, realPosition, offsetTime);
			//player.transform.rotation = Quaternion.Lerp(player.transform.rotation, realRotation, offsetTime);
			
			hat.transform.position = Vector3.Lerp(hat.transform.position, realPositionHat, .09f);
			hat.transform.rotation = Quaternion.Lerp(hat.transform.rotation, realRotationHat, .09f);
			
			beard.transform.position = Vector3.Lerp(beard.transform.position, realPositionBeard, .09f);
			beard.transform.rotation = Quaternion.Lerp(beard.transform.rotation, realRotationBeard, .09f);
			
			center.transform.position = Vector3.Lerp(center.transform.position, realPositionCenter, .09f);
			center.transform.rotation = Quaternion.Lerp(center.transform.rotation, realRotationCenter, .09f);

		} 
	}

	void FixedUpdate ()
	{
		if (!photonView.isMine) {
			player.GetComponent<Rigidbody> ().position = Vector3.Lerp (player.GetComponent<Rigidbody> ().position, realPositionR, .09f)+ realVelocityR*Time.deltaTime *offsetTime;
			player.GetComponent<Rigidbody> ().velocity = Vector3.Lerp (player.GetComponent<Rigidbody> ().velocity, realVelocityR, .09f)+ realVelocityR*Time.deltaTime *offsetTime;
			player.GetComponent<Rigidbody> ().rotation = Quaternion.Lerp (player.GetComponent<Rigidbody> ().rotation, realRotationR, .09f);
			player.GetComponent<Rigidbody> ().angularVelocity = Vector3.Lerp (player.GetComponent<Rigidbody> ().angularVelocity, realVelocityRA, .09f)+ realVelocityR*Time.deltaTime*offsetTime;
		}
	}
	
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo inf) {
		if (stream.isWriting) {
			stream.SendNext(player.transform.position);
			stream.SendNext(player.transform.rotation);
			stream.SendNext(player.GetComponent<Rigidbody>().position);
			stream.SendNext(player.GetComponent<Rigidbody>().rotation);
			stream.SendNext(player.GetComponent<Rigidbody>().velocity);
			stream.SendNext(player.GetComponent<Rigidbody>().angularVelocity);
			stream.SendNext(player.GetComponent<HealthPoints>().getHP());
			
			stream.SendNext(hat.transform.position);
			stream.SendNext(hat.transform.rotation);
			
			stream.SendNext(beard.transform.position);
			stream.SendNext(beard.transform.rotation);
			
			stream.SendNext(center.transform.position);
			stream.SendNext(center.transform.rotation);
		}
		else
		{
			oldPosition = realPosition;
			offsetTime = 0;
			Sync = true;
			networkTime = inf.timestamp;
			realPosition = (Vector3) stream.ReceiveNext();
			realRotation = (Quaternion) stream.ReceiveNext();
			realPositionR = (Vector3)stream.ReceiveNext();
			realRotationR = (Quaternion)stream.ReceiveNext();
			realVelocityR = (Vector3)stream.ReceiveNext();
			realVelocityRA = (Vector3)stream.ReceiveNext();
			oldHealth = Health;
			Health = (int)stream.ReceiveNext();
			
			realPositionHat = (Vector3) stream.ReceiveNext();
			realRotationHat = (Quaternion) stream.ReceiveNext();
			
			realPositionBeard = (Vector3) stream.ReceiveNext();
			realRotationBeard = (Quaternion) stream.ReceiveNext();
			
			realPositionCenter = (Vector3) stream.ReceiveNext();
			realRotationCenter = (Quaternion) stream.ReceiveNext();
			
			if(Health != oldHealth )
			{
				player.GetComponent<HealthPoints>().OnHealthChanged();
			}
		}
	}
}
