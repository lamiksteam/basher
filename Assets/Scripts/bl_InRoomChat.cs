//------------------------------------------------------------------
//Simple example of chat from Photon.
//
//                Lovatto Studio
//------------------------------------------------------------------
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PhotonView))]
public class bl_InRoomChat : Photon.MonoBehaviour 
{
    public GUISkin m_Skin;
    [Space(5)]
    public Rect GuiRect = new Rect(0,0, 250,300);
    public bool IsVisible = true;
    public bool AlignBottom = false;
    public bool WithSound = true;
    public int MaxMsn = 7;
    public List<string> messages = new List<string>();
    private string inputLine = "";
    private Vector2 scrollPos = Vector2.zero;
    [Space(5)]
    public AudioClip MsnSound;
    
    public static readonly string ChatRPC = "Chat";
    private float m_alpha = 2f;
    private static bl_InRoomChat instance;
    private bool isChat = false;

    public static bl_InRoomChat Instance
    {

        get
        {
            return instance ?? (instance = GameObject.FindObjectOfType(typeof(bl_InRoomChat)) as bl_InRoomChat);
        }

    }

    public void OnGUI()
    {
            if (m_alpha > 0.0f && !isChat)
            {
                m_alpha -= Time.deltaTime/2;
            }
            else if (isChat)
            {
                m_alpha = 10;
            }
            if (this.AlignBottom)
            {
                this.GuiRect.y = (Screen.height - this.GuiRect.height) - GuiRect.y;
            }
            GUI.skin = m_Skin;
            GUI.color = new Color(1, 1, 1, m_alpha);
            if (!this.IsVisible || PhotonNetwork.connectionStateDetailed != PeerState.Joined)
            {
                return;
            }

            if (Event.current.type == EventType.KeyDown && (Event.current.keyCode == KeyCode.KeypadEnter || Event.current.keyCode == KeyCode.Return))
            {
                if (!string.IsNullOrEmpty(this.inputLine) && isChat && Screen.lockCursor)
                {
                    this.photonView.RPC("Chat", PhotonTargets.All, this.inputLine);
                    this.inputLine = "";
                    GUI.FocusControl("");
                    isChat = false;
                    return; // printing the now modified list would result in an error. to avoid this, we just skip this single frame
                }
                else if (!isChat && Screen.lockCursor)
                {
                    GUI.FocusControl("ChatInput");
                    isChat = true;
                }
                else
                {
                    if (isChat)
                    {
                        Closet();
                    }
                }
            }

            GUI.SetNextControlName("");
            GUILayout.BeginArea(this.GuiRect);

            scrollPos = GUILayout.BeginScrollView(scrollPos);
            GUILayout.FlexibleSpace();
            for (int i = messages.Count - 1; i >= 0; i--)
            {
                GUILayout.Label(messages[i]);
            }
            GUILayout.EndScrollView();

            GUILayout.BeginHorizontal();
            GUI.SetNextControlName("ChatInput");
            inputLine = GUILayout.TextField(inputLine);
            if (GUILayout.Button("Send", "box", GUILayout.ExpandWidth(false)))
            {
                this.photonView.RPC("Chat", PhotonTargets.All, this.inputLine);
                this.inputLine = "";
                GUI.FocusControl("");
            }
            GUILayout.EndHorizontal();
            GUILayout.EndArea();
   
    }

    void Closet()
    {
        isChat = false;
        GUI.FocusControl("");
    }

    [PunRPC]
    public void Chat(string newLine, PhotonMessageInfo mi)
    {
        m_alpha = 7;
        string senderName = "anonymous";

        if (mi != null && mi.sender != null)
        {
            if (!string.IsNullOrEmpty(mi.sender.name))
            {
                senderName = mi.sender.name;
            }
            else
            {
                senderName = "player " + mi.sender.ID;
            }
        }

        this.messages.Add("[" + senderName + "]: " + newLine);
        if (MsnSound != null && WithSound)
        {
            GetComponent<AudioSource>().PlayOneShot(MsnSound);
        }
    }

    public void AddLine(string newLine)
    {
        this.messages.Add(newLine);
        if (messages.Count > MaxMsn)
        {
            messages.RemoveAt(0);
        }
    }
}