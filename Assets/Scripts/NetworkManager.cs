﻿using UnityEngine;
using System.Collections;
using System;

public class NetworkManager : MonoBehaviour {
	private int roomNr = 0;
	public bool offline;
	private GameObject[] SpawnSpots;
	// Use this for initialization
	void Start () {
		SpawnSpots = GameObject.FindGameObjectsWithTag ("SpawnSpot");
		if (!offline) {
			PhotonNetwork.ConnectUsingSettings ("0.1");
			roomNr++;
		} 
		else {
			PhotonNetwork.offlineMode = true;
			OnJoinedRoom();
		}
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	private const string roomName = "room ";
	private RoomInfo[] roomsList;
	
	void OnGUI()
	{
		GUILayout.Space(10);
		if (PhotonNetwork.isMasterClient)
		{
			GUILayout.Label("Ping: " + PhotonNetwork.GetPing());
		}
		else if (PhotonNetwork.isNonMasterClientInRoom)
		{
			GUILayout.Label("Ping: " + PhotonNetwork.GetPing());
		}
		else
		{
			GUILayout.Label("Not connected..." + PhotonNetwork.connectionStateDetailed);
		}

		if (!PhotonNetwork.connected)
		{
			GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
		}
		else if (PhotonNetwork.room == null)
		{
			// Create Room
			if (GUI.Button(new Rect(Screen.width/2 - 125, Screen.height/4, 250, 100), "Start Server"))
			{
				PhotonNetwork.CreateRoom(roomName + roomNr.ToString());
				roomNr++;
			}

			// Join Room
			if (roomsList != null)
			{
				for (int i = 0; i < roomsList.Length; i++)
				{
					if (GUI.Button(new Rect(Screen.width/2 - 125, Screen.height/4 + (110 * (i+1)), 250, 100), "Join " + roomsList[i].name))
						PhotonNetwork.JoinRoom(roomsList[i].name);
				}
			}
		}
	}
	
	void OnReceivedRoomListUpdate()
	{
		roomsList = PhotonNetwork.GetRoomList();
	}
	void OnJoinedRoom()
	{
		GameObject mySpawnSpot = SpawnSpots [UnityEngine.Random.Range (0, SpawnSpots.Length)];
		object[] name = new object[1];
		name[0] = "Lavar";
		GameObject gm = PhotonNetwork.Instantiate ("Actor", mySpawnSpot.transform.position, mySpawnSpot.transform.rotation, 0, name);
		Transform tr = gm.transform.FindChild ("BasherCamera");
        gm.GetComponentInChildren<MouseLook>().enabled = true;
        gm.GetComponentInChildren<Movement>().enabled = true;
        gm.GetComponentInChildren<HealthPoints>().enabled = true;
        gm.GetComponent<Shooting>().enabled = true;
        tr.gameObject.SetActive(true);
        //GameObject gmC = gm.transform.FindChild ("Cube").gameObject;
        //gmC.GetComponentInChildren<Test> ().enabled = true;
        //gmC.GetComponent<FollowMaster>().enabled = true;
        //gm.GetComponentInChildren<Move> ().enabled 	= true;
        //gm.GetComponentInChildren<MouseLook> ().enabled = true;
		//gm.GetComponentInChildren<HealthPoints> ().enabled = true;
		//gm.GetComponent<Shooting> ().enabled = true;
        
	}
}