﻿using UnityEngine;
using System.Collections;

public class lastHope : MonoBehaviour
{
    public GameObject p;
    public GameObject h;
    public double m_InterpolationBackTime = 0.1;
    public double m_ExtrapolationLimit = 0.5;
    public float interpolationConstant = 0.05f;        // &lt;---- This was originally .1, but I had to make it .01 to really get rid of the jitter

    internal struct State
    {
        internal double timestamp;
        internal Vector3 pos;
        internal Vector3 velocity;
        internal Quaternion rot;
        internal Vector3 angularVelocity;
    }

    // We store twenty states with "playback" information
    State[] m_BufferedState = new State[20];
    State[] h_BufferedState = new State[20];
    // Keep track of what slots are used
    int m_TimestampCount;
    PhotonView _pv;

    void Awake()
    {
        _pv = (PhotonView)gameObject.GetComponent("PhotonView");         // &lt;--- used to check if networkView.isMine
        enabled = !_pv.isMine;
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        // Send data to server
        if (stream.isWriting)
        {
            Vector3 pos = p.transform.position;
            Quaternion rot = p.transform.rotation;
            Vector3 velocity = p.GetComponent<Rigidbody>().velocity;
            Vector3 angularVelocity = p.GetComponent<Rigidbody>().angularVelocity;

            Vector3 posHat = h.transform.position;
            Quaternion rotHat = h.transform.rotation;

            stream.Serialize(ref pos);
            stream.Serialize(ref velocity);
            stream.Serialize(ref rot);
            stream.Serialize(ref angularVelocity);

            stream.Serialize(ref posHat);
            stream.Serialize(ref rotHat);
        }
        // Read data from remote client
        else
        {
            Vector3 pos = Vector3.zero;
            Vector3 velocity = Vector3.zero;
            Quaternion rot = Quaternion.identity;
            Vector3 angularVelocity = Vector3.zero;

            Vector3 posHat = Vector3.zero;
            Quaternion rotHat = Quaternion.identity;

            stream.Serialize(ref pos);
            stream.Serialize(ref velocity);
            stream.Serialize(ref rot);
            stream.Serialize(ref angularVelocity);

            stream.Serialize(ref posHat);
            stream.Serialize(ref rotHat);

            // Shift the buffer sideways, deleting state 20
            for (int i = m_BufferedState.Length - 1; i >= 1; i--)
            {
                m_BufferedState[i] = m_BufferedState[i - 1];
            }

            for (int j = h_BufferedState.Length - 1; j >= 1; j--)
            {
                h_BufferedState[j] = h_BufferedState[j - 1];
            }

            // Record current state in slot 0
            State state;
            state.timestamp = info.timestamp;
            state.pos = pos;
            state.velocity = velocity;
            state.rot = rot;
            state.angularVelocity = angularVelocity;
            m_BufferedState[0] = state;

            State stateHat;
            stateHat.timestamp = info.timestamp;
            stateHat.pos = posHat;
            stateHat.rot = rotHat;
            stateHat.velocity = velocity;
            stateHat.angularVelocity = angularVelocity;
            h_BufferedState[0] = stateHat;

            // Update used slot count, however never exceed the buffer size
            // Slots aren't actually freed so this just makes sure the buffer is
            // filled up and that uninitalized slots aren't used.
            m_TimestampCount = Mathf.Min(m_TimestampCount + 1, m_BufferedState.Length);

            // Check if states are in order, if it is inconsistent you could reshuffel or
            // drop the out-of-order state. Nothing is done here
            for (int i = 0; i < m_TimestampCount - 1; i++)
            {
                if (m_BufferedState[i].timestamp < m_BufferedState[i + 1].timestamp)
                    Debug.Log("State inconsistent");
            }
        }
    }

    // We have a window of interpolationBackTime where we basically play
    // By having interpolationBackTime the average ping, you will usually use interpolation.
    // And only if no more data arrives we will use extra polation
    void FixedUpdate()
    {

        if (m_BufferedState[0].timestamp == 0)
            return;
        if (h_BufferedState[0].timestamp == 0)
            return;

        // This is the target playback time of the rigid body
        double interpolationTime = PhotonNetwork.time - m_InterpolationBackTime;

        // Use interpolation if the target playback time is present in the buffer
        if (m_BufferedState[0].timestamp > interpolationTime)
        {

            // Go through buffer and find correct state to play back
            for (int i = 1; i < m_TimestampCount; i++)
            {
                if (m_BufferedState[i].timestamp <= interpolationTime || i == m_TimestampCount - 1)
                {
                    // The state one slot newer (&lt;100ms) than the best playback state
                    State rhs = m_BufferedState[i - 1];
                    // The best playback state (closest to 100 ms old (default time))
                    State lhs = m_BufferedState[i];

                    State hrhs = h_BufferedState[i - 1];
                    State hlhs = h_BufferedState[i];

                    // Use the time between the two slots to determine if interpolation is necessary
                    double length = rhs.timestamp - lhs.timestamp;
                    float t = 0.0F;
                    // As the time difference gets closer to 100 ms t gets closer to 1 in
                    // which case rhs is only used
                    // Example:
                    // Time is 10.000, so sampleTime is 9.900
                    // lhs.time is 9.910 rhs.time is 9.980 length is 0.070
                    // t is 9.900 - 9.910 / 0.070 = 0.14. So it uses 14% of rhs, 86% of lhs
                    if (length > 0.0001)
                    {
                        t = (float)((interpolationTime - lhs.timestamp) / length);
                    }

                    // if t=0 =&gt; lhs is used directly
                    p.transform.position =
                        Vector3.Lerp(p.transform.position,
                                      Vector3.Lerp(lhs.pos, rhs.pos, t),
                                      interpolationConstant);
                    //h.transform.position =
                    //    Vector3.Lerp(h.transform.position,
                    //                  Vector3.Lerp(hlhs.pos, hrhs.pos, t),
                    //                  interpolationConstant);
                    p.transform.rotation =
                        Quaternion.Slerp(p.transform.rotation,
                                          Quaternion.Slerp(lhs.rot, rhs.rot, t),
                                          interpolationConstant);
                    //h.transform.rotation =
                    //    Quaternion.Slerp(h.transform.rotation,
                    //                      Quaternion.Slerp(hlhs.rot, hrhs.rot, t),
                    //                      interpolationConstant);
                    p.GetComponent<Rigidbody>().velocity =
                        Vector3.Lerp(p.GetComponent<Rigidbody>().velocity,
                                      Vector3.Lerp(lhs.velocity, rhs.velocity, t),
                                      interpolationConstant);
                    p.GetComponent<Rigidbody>().angularVelocity =
                        Vector3.Lerp(p.GetComponent<Rigidbody>().angularVelocity,
                                      Vector3.Lerp(lhs.angularVelocity, rhs.angularVelocity, t),
                                      interpolationConstant);
                    return;
                }
            }
        }
        else
        {

            float dt = (float)(PhotonNetwork.time - m_BufferedState[0].timestamp);
            float dh = (float)(PhotonNetwork.time - h_BufferedState[0].timestamp);
            Vector3 extra_pos = m_BufferedState[0].pos + m_BufferedState[0].velocity * dh;

            float angle = m_BufferedState[0].angularVelocity.magnitude;
            Vector3 axis = m_BufferedState[0].angularVelocity / angle;
            Quaternion extra_rot = m_BufferedState[0].rot * Quaternion.AngleAxis(angle * dt, axis);

            p.transform.position = Vector3.Lerp(p.transform.position, extra_pos, interpolationConstant);
            p.transform.rotation = Quaternion.Slerp(p.transform.rotation, extra_rot, interpolationConstant);
            h.transform.position = Vector3.Lerp(p.transform.position, new Vector3(extra_pos.x, extra_pos.y, extra_pos.z), interpolationConstant);
            h.transform.rotation = Quaternion.Slerp(h.transform.rotation, h_BufferedState[0].rot, interpolationConstant);

            p.GetComponent<Rigidbody>().angularVelocity = Vector3.Lerp(p.GetComponent<Rigidbody>().angularVelocity, m_BufferedState[0].angularVelocity, interpolationConstant);
        }
    }
}