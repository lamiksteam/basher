﻿using UnityEngine;
using System.Collections;

public class Shooting : Photon.MonoBehaviour {
	public float coolDown;
	private float timeStamp = 0;
	public Transform startPoint;
	public GameObject rocketFire;
	public Vector3 position;
	public Quaternion rotation;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButton ("Fire1") && timeStamp <= Time.time) {
			position = startPoint.position;
			rotation = startPoint.rotation;
			photonView.RPC ("Shot", PhotonTargets.All, position, rotation);
			timeStamp = Time.time + coolDown;
		}

	}

	[PunRPC]
	void Shot(Vector3 p, Quaternion r){
		if (this.enabled || !this.enabled) {
			GameObject rocket = (GameObject)Instantiate (rocketFire, p, r);
			rocket.GetComponent<RocketMove> ().setOwner (PhotonNetwork.playerName);
		}
	}	
}
