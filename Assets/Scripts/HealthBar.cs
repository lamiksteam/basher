﻿using UnityEngine; 
using System.Collections;

public class HealthBar : MonoBehaviour { 
	
	public int maxHealth=100; 
	public int curHealth=50; 
	public Vector3 screenPosition;
	public float healthBarLenght;
	
	void Start () { 
		healthBarLenght=Screen.width/19; 
	}
	
	void Update () {
		screenPosition = Camera.main.WorldToScreenPoint(transform.position);
		screenPosition.y = Screen.height - screenPosition.y;
		AddjustCurrentHealth(100);
	} 

	void OnGUI(){
		//GUI.Box(
		GUI.Box(new Rect(screenPosition.x * 0.946f,screenPosition.y * 0.85f , healthBarLenght, screenPosition.x * 0.018f), string.Empty);
		//GUI.Box(new Rect(screenPosition.x * 0.946f,screenPosition.y * 0.85f , healthBarLenght, screenPosition.x * 0.018f), string.Empty);
	}
	
	public void AddjustCurrentHealth(int adj){ 
		curHealth += adj;
		if(curHealth < 1)
			curHealth = 0;
		if(curHealth >maxHealth)
			curHealth=maxHealth;
		if(maxHealth < 1)
			maxHealth = 1;
		healthBarLenght=(Screen.width/19)*(curHealth/(float)maxHealth);
	}
}
